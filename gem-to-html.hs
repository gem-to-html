-- copyright juniper
-- license: do whatever you want with it, it's not good code
-- if you do screw around with it, please let me know.

-- bugs: < gemtext is written as < intead of the proper html of &lt;


import System.Environment as Env
import System.IO as M
import System.Directory as D
import Data.List as L
import System.FilePath.Posix as P
-- import System.Exit as Exit



main :: IO ()
main = do
  args <- Env.getArgs
--  putStrLn "The arguments are:"
--  mapM putStrLn args
  let
    task = parseArguments args
  handleTask task

-- given certain arguments, the program has certain tasks to do
handleTask :: WhatToDo -> IO ()
handleTask Empty = putStr emptyWarning
handleTask SyntaxError = putStr syntaxError
handleTask Help = putStr usage
handleTask Version = putStr version
handleTask (SingleFile (InFilePath fileIn) (OutFilePath fileOut)) = handleSingleParse fileIn fileOut
handleTask (MultiFile flags (InDir dirIn) (OutDir dirOut)) = handleMultiParse flags dirIn dirOut



handleSingleParse :: M.FilePath -> M.FilePath -> IO ()
handleSingleParse filePathIn filePathOut = do
  inputHandle <- openFile filePathIn ReadMode
  gemtext <- hGetContents inputHandle
  let
    html = gemtextToHtml gemtext
  outputHandle <- openFile filePathOut WriteMode
  hPutStr outputHandle html
  hClose inputHandle
  hClose outputHandle


handleMultiParse :: MultiFileFlags -> M.FilePath -> M.FilePath -> IO ()
handleMultiParse allFlags@(MultiFileFlags { rFlag = Flag isRFlag
                                          , xFlag = Flag isXFlag})
                 dirPathIn dirPathOut =
  do
    files <- D.listDirectory dirPathIn
    absInPath <- D.makeAbsolute dirPathIn
    absOutPath <- D.makeAbsolute dirPathOut
    let

      -- ensures all paths are relative!
      relativeFiles = map (P.makeRelative absInPath) files
      absoluteFiles = map (absInPath P.</>) relativeFiles
--    print "multi file parsing!"
--    print absInPath
--    print absOutPath
--    print isRFlag
--    print isXFlag
    (directories, gemfiles, extras) <- sortFilesIntoDirsGemExtras absoluteFiles


    -- ensure the target directory exists and create it if it doesn't
    -- create parents too if missing
    D.createDirectoryIfMissing True absOutPath
      

    -- parse all gemfiles in this directory
    let
      makeHtmlExtension = (P.<.> "html") . dropExtension
      sourceGems = gemfiles --absolute
      relativeGems = map (P.makeRelative absInPath) sourceGems --relative
      targetHtmls = map (absOutPath P.</>) (map makeHtmlExtension relativeGems)
    -- handleSingleParse takes a single file and produces outputfile->IO()
    -- the map expression is a list of [outputfile -> IO()]
    -- zipping with the list of targetHtml using function application yields a list of IO()
    -- sequence turns this into a single IO action

--    print "Parsing gemini files"
--    print (zip sourceGems targetHtmls)
    sequence (zipWith ($) (map handleSingleParse sourceGems) targetHtmls)



    let
      sourceExtras = extras --absolute
      relativeExtras = map (P.makeRelative absInPath) sourceExtras --relative
      targetExtras = map (absOutPath P.</>) relativeExtras --absolute
    -- should we copy all the "extras"?
    if isXFlag
      then do
        sequence (zipWith ($) (map D.copyFile sourceExtras) targetExtras)
        return ()
--        print "copying extra files!"
--        print (zip sourceExtras targetExtras)
      else return ()


    -- if we recurse, then run this code again but with updated paths for the new subfolders

  
    return ()

    
    if isRFlag
      then do
        let
          sourceDirs = directories --absolute
          relativeDirs = map (P.makeRelative absInPath) directories
          targetDirs = map (absOutPath P.</>) relativeDirs
--        print "recursing!"
--        print (zip sourceDirs targetDirs)
        sequence (zipWith ($) (map (handleMultiParse allFlags) sourceDirs) targetDirs)
        -- the above has type [()] which is why we discard it and return () below
        return ()
      else return ()

    return ()



sortFilesIntoDirsGemExtras :: [M.FilePath] -> IO ([M.FilePath], [M.FilePath], [M.FilePath])
-- input is absolute filepaths!
sortFilesIntoDirsGemExtras [] = return ([], [], [])
sortFilesIntoDirsGemExtras (f:fs) = do
  isDir <- D.doesDirectoryExist f
  isFile <- D.doesFileExist f
  let
    isGem = hasGMIExtension f
  (a,b,c) <- sortFilesIntoDirsGemExtras fs
  if isDir
    then return (f:a, b, c)
    else if isFile && isGem
         then return (a, f:b, c)
         else if isFile
              then return (a, b, f:c)
              else error ("File doesn't exist! " ++ f)


hasGMIExtension :: M.FilePath -> Bool
hasGMIExtension path = longEnough && rightEnding
  where
    len = length path
    longEnough = (len >= 4)
    rightEnding = (drop (len - 4) path) == ".gmi"
  

data Flag = Flag Bool
data MultiFileFlags = MultiFileFlags { rFlag :: Flag
                                     , xFlag :: Flag
                                     }

data InFilePath = InFilePath M.FilePath
data OutFilePath = OutFilePath M.FilePath
data InDirectoryPath = InDir M.FilePath
data OutDirectoryPath = OutDir M.FilePath
data WhatToDo = SyntaxError | Help | Version | SingleFile InFilePath OutFilePath | MultiFile MultiFileFlags InDirectoryPath OutDirectoryPath | Empty


parseArguments :: [String] -> WhatToDo
-- take a list of arguments and says what to do
parseArguments argList
  | null argList            = Empty
  | "-h" `elem` argList     = Help
  | "-v" `elem` argList     = Version
  | not $ validArgs argList = SyntaxError
  | isSingleFile argList    = (SingleFile (InFilePath (inFilePath argList)) (OutFilePath (outFilePath argList)))
  | isMultiFile argList     = (MultiFile
                               (MultiFileFlags { rFlag = Flag (hasRecurseFlag $ nonIOArgs argList)
                                               , xFlag = Flag (hasExtraFlag $ nonIOArgs  argList)
                                               })
                               (InDir (inDirPath argList))
                               (OutDir (outDirPath argList)))
  | otherwise               = error "failed to validate arguments"



hasRecurseFlag :: [String] -> Bool
hasRecurseFlag list = any ($ list) [elem "-r", elem "-rx", elem "-xr"]

hasExtraFlag :: [String] -> Bool
hasExtraFlag list = any ($ list) [elem "-x", elem "-rx", elem "-xr"]

isSingleFile :: [String] -> Bool
-- returns true if the argument list is of type -i/-o and false otherwise
-- assume argument list is valid
-- note that gem-to-html -x -I -i -r -O -o yields False
isSingleFile args =
  let
    singleFlags = ["-i", "-o"]
  in
    (args !! 0 `elem` singleFlags)



isMultiFile :: [String] -> Bool
-- returns true if the argument list is of type -I/-O and false otherwise
-- assume argument list is valid
-- note that gem-to-html -I -i -r -O -o yields True, but
-- gem-to-html -i -I -o -O yields false
-- to distinguish these, the first argument that's -i or -I must be in fact -I
isMultiFile args =
  if "-i" `elem` args && "-I" `elem` args
  then
    (findFirstIndex "-I" args) > (findFirstIndex "-i" args)
  else
    "-I" `elem` args




inFilePath :: [String] -> M.FilePath
-- takes an argument list and returns the argument after -i as a (System.IO) FilePath
-- assume valid argument list, so
-- the format MUST be -i F -o G or -o F -i G
inFilePath argList =
  if (head argList) == "-i"
  then argList !! 1
  else argList !! 3


outFilePath :: [String] -> M.FilePath
-- takes an argument list and returns the argument after -o as a (System.IO) FilePath
-- assume valid argument list, so
-- the format MUST be -i F -o G or -o F -i G
outFilePath argList = 
  if (head argList) == "-o"
  then argList !! 1
  else argList !! 3



inDirPath :: [String] -> M.FilePath
-- takes an argument list and returns the argument after -I as a (System.IO) FilePath
-- drops a trailing path separator
inDirPath = P.dropTrailingPathSeparator . (!! 1) . (dropWhile' (/= "-I"))


outDirPath :: [String] -> M.FilePath
-- takes an argument list and returns the argument after -O as a (System.IO) FilePath
-- drops a trailing path separator
outDirPath = P.dropTrailingPathSeparator . (!! 1) . (dropWhile' (/= "-O"))



validArgs :: [String] -> Bool
-- validate the argument list
-- only weird condition is type -I/-O, where we must make sure
  -- there is exactly one of -I and -O (and each has a following argument which exists and isn't -I nor -O)
  -- all other arguments besides -I and -O are -r, -x, -rx, or, -xr, with nothing else and nothing redundant
-- note, the command "gem-to-html -I -r -O -x" IS valid and has ZERO flags set besides -I and -O.
validArgs ["-h"] = True
validArgs ["-v"] = True
validArgs ["-i", _, "-o", _] = True
validArgs ["-o", _, "-i", _] = True
validArgs argList = hasInAndOut argList && otherArgsValid argList



hasInAndOut :: [String] -> Bool
-- is there precisely one -I and one -O in the arg list?
-- is each followed by at least one argument (which we'll read as a file path), i.e., is neither at the end?
-- we cannot have [...,"-I","-O",...] nor [...,"-O","-I",...]
hasInAndOut argList =
  (count "-I" argList == 1) &&
  (count "-O" argList == 1) &&
  (not $ (last argList) `elem`  ["-I","-O"]) &&
  (not $ L.isInfixOf ["-I","-O"] argList) &&
  (not $ L.isInfixOf ["-O","-I"] argList)

  
otherArgsValid :: [String] -> Bool
-- verifies that if we delete -I and the argument after it, and -O and the argument after that
-- the remaining arguments are exactly one of the following lists precisely
-- [], ["-r"], ["-x"], ["-rx"], ["-xr"], ["-r", "-x"], ["-x", "-r"]
otherArgsValid argList =
  let
    otherArgs = nonIOArgs argList
  in
    otherArgs `elem` [[],
                      ["-r"],
                      ["-x"],
                      ["-rx"],
                      ["-xr"],
                      ["-r", "-x"],
                      ["-x", "-r"]]
    

    
nonIOArgs :: [String] -> [String]
-- returns the argument list sans -I, -O and corresponding paths
nonIOArgs argList =
  let
    indexOfI = findFirstIndex "-I" argList
    indexOfO = findFirstIndex "-O" argList
  in
    dropIndicesFromList [indexOfI, indexOfI + 1, indexOfO, indexOfO + 1] argList

    

dropIndexFromList :: (Eq a) => Int -> [a] -> [a]
dropIndexFromList 0 (x:xs) = xs
dropIndexFromList n list = dropIndexFromList (n-1) list

dropIndicesFromList :: (Eq a) => [Int] -> [a] -> [a]
dropIndicesFromList = dropIndicesFromListHelper 0


dropIndicesFromListHelper :: (Eq a) => Int -> [Int] -> [a] -> [a]
-- first argument is the offset, i.e., number of elements we imagine are dropped from the [a] list
dropIndicesFromListHelper _ _ [] = []
dropIndicesFromListHelper _ [] list = list
dropIndicesFromListHelper n indexList (x:xs) =
  if n `elem` indexList
  then dropIndicesFromListHelper (n + 1) indexList xs
  else x:(dropIndicesFromListHelper (n + 1) indexList xs)



findFirstIndex :: (Eq a) => a -> [a] -> Int
findFirstIndex _ [] = error "element is not in list!"
findFirstIndex c (x:xs) = if c == x
                        then 0
                        else 1 + findFirstIndex c xs
                             

count :: (Eq a) => a -> [a] -> Int
count _ [] = 0
count c (x:xs) = if c == x
               then 1 + count c xs
               else count c xs

    

argsAreRX :: [String] -> Bool
-- returns true if -r and -x or -rx or -xr is an argument
argsAreRX argList
  | "-r" `elem` argList && "-x" `elem` argList = True
  | "-rx" `elem` argList                       = True
  | "-xr" `elem` argList                       = True
  | otherwise                                  = False





emptyWarning = "Please supply some arguments.  For help, see gem-to-html -h\n"

version = "gem-to-html version 0.21"

syntaxError :: String
syntaxError = "I cannot parse the arguments you used.  This is a syntax error.\n\nPlease read the help with gem-to-html -h\n"

usage :: String
usage = "gem-to-html is a converter from gemini text (gemtext) to html\n\
\\n\
\Usage:\n\
\gem-to-html [-h|-v]\n\
\gem-to-html -i [SOURCE FILE] -o [TARGET FILE]\n\
\gem-to-html [-r|-x]... -I [SOURCE DIRECTORY] -O [TARGET DIRECTORY]\n\
\\n\
\  -h  show the Help\n\
\  -v  show the Version\n\
\  -i  input file (converts only this single file which is assumed to be gemtext)\n\
\  -o  output file (only used with -i as the destination for a single converted file)\n\
\      this WILL overwrite files\n\
\  -I  Input directory (converts all files ending with .gmi inside this directory, recursing deeper if -r only if is used)\n\
\      this REQUIRES gemtext to end in .gmi\n\
\  -O  Output directory (sends all files targeted by -I to this directory, recreating subfolders if -r is used.  If the directory doesn't exist, it will be created even if -r isn't used.)\n\
\      this WILL change .gmi file endings to .html\n\
\  -r  Recurse when using -I or -O.  Will recreate entire subdirectory tree, even empty folders.\n\
\  -x  add eXtras (copies files not ending with .gmi into target directory, for use with -I only, otherwise files not ending in .gmi are ignored)\n\
\  [-rx|-xr] are both supported to mean -r and -x\n\
\\n\
\Note: -i/-I and -o/-O can go in either order\n\
\\n\
\\n\
\Examples:\n\
\gem-to-html -i gemini-file.txt -o html-file.html\n\
\will parse the gemtext file gemini-file.txt and save the result as html-file.html\n\
\\n\
\gem-to-html -r -x -I gem -O html\n\
\where gem has the files:\n\
\gem/index.gmi\n\
\gem/news/index.gmi\n\
\gem/docs/document.txt\n\
\gem/text.txt\n\
\\n\
\will create the following files:\n\
\html/index.html\n\
\html/test.txt\n\
\html/news/\n\
\html/news/index.html\n\
\html/docs/\n\
\html/docs/document.txt\n\
\\n\
\where the .gmi files have been parsed and the .txt files copied over directly\n\
\\n\
\Without -x, the document.txt file will not be created, but html/docs WILL be if -r is specified\n\
\Without -r, only index.html and test.txt will be created .\n\
\With neither flag, only index.gmi is created.\n\
\\n\
\Warnings:\n\
\This program doesn't handle flags well, so the syntax is delicate.  Lots of things will throw errors.\n\
\\n\
\THIS PROGRAM WILL OVERWRITE FILES WITHOUT WARNING!\n\
\I ALSO CAN'T WRITE HASKELL SO ASSUME THIS WILL BREAK YOUR COMPUTER!\n\
\THIS IS LITERALLY THE FIRST \"BIG\" HASKELL PROGRAM I'VE WRITTEN SO IT IS NOT GOOD\n\
\\n\
\\n\
\\n\
\YOU HAVE BEEN WARNED!\n"




gemtextToHtml :: String -> String
-- takes entire gemtext document to html
gemtextToHtml = wrappedParser . (splitOn' '\n')



splitOn' :: (Eq a) => a -> [a] -> [[a]]
-- NOT the built-in splitOn
-- takes an element and a list, splits it using the element as a delimiter (deleting it from the output)
-- imagine every delim is a newline.  splitOn' then prints each line as its own list
-- splitOn' 2 [1,3,4,2,3,2,1,2,2,0] = [[1,3,4],[3],[1],[],[0]]
-- WARNING: the last line is assumed to NOT be delim-ended, so
-- splitOn' 2 [2,0,2,2] = [[],[0],[],[]]
splitOn' _ [] = []
splitOn' delim list = foldr (\x acc@(y:ys) ->
                               if x == delim
                               then []:acc
                               else (x:y):ys)
                           [[]] list




wrappedParser :: [String] -> String
-- sends list of gemtext lines to html document, adding an html header and footer
wrappedParser gemtextList = htmlHeader ++ (parseBody gemtextList) ++ htmlFooter
  where
    htmlHeader = "<html>\n" ++ head ++ "<body>"
    head = ""
--    head = "<head>\n<title>" ++ title ++ "</title>\n</head>\n"
--    title = "Testing?"
    htmlFooter = "\n" ++ "</body>\n</html>"

parseBody :: [String] -> String
-- takes an entire gemtext document and returns html body
parseBody text = parserHelper text "" False False


parserHelper :: [String] -> String -> Bool -> Bool -> String
-- expects a list of lines of gemtext and a current body of html to append to
-- also takes two booleans, whether a verbatim tag is open, and whether a <li> has been opened
parserHelper [] _ True _  = error "Forgot to close pre tags in file!"
parserHelper [] body _ True  = body ++ "\n" ++ "</ul>"
  -- ended the document with a list line
parserHelper [] body _ _ = body
parserHelper (line:rest) body isVerbatim isList
  | isVerbatim =
      if (take 3 line) == "```"
      then parserHelper rest (body ++ "\n" ++ "</pre>") False isList
           -- end pre tag for preformatted text
      else parserHelper rest (body ++ "\n" ++ line) True isList
           -- just keep chucking stuff in the preformatted tags
  | isList =
      if (take 2 line) == "* "
         -- still a list!
      then parserHelper rest (body ++ "\n" ++ parseLine line) False True
      else parserHelper rest (body ++ "\n" ++ "</ul>" ++ "\n" ++ parseLine line) False False
  | otherwise =
      if (take 3 line) == "```"
      then
        -- opening a pre tag!
        parserHelper rest (body ++ "\n" ++ "<pre>") True False
      else
        -- no pre tags opened
        if (take 2 line) == "* "
           -- beginning new list
        then parserHelper rest (body ++ "\n" ++ "<ul>" ++ "\n" ++ parseLine line) False True
        else parserHelper rest (body ++ "\n" ++ parseLine line) False False



parseLine :: String -> String
-- takes a line of gemtext and prints the corresponding raw line of html
-- list lines become the line wrapped in <ul> </ul>
parseLine line
  | isLinkTag tag = parseLinkLine line
  | isHeaderTag tag = parseHeaderLine line
  | isListTag tag = parseListLine line
  | isQuoteTag tag = parseQuoteLine line
  | otherwise = parseParagraphLine line -- normal text
  where
    tag = (take 4 line) -- only ever need to check first four characters
    isLinkTag ('=':'>':rest) = isWhiteSpace (head rest)
    isLinkTag _ = False
    isHeaderTag string = (take 2 string == "# ") || (take 3 string == "## ") || (take 4 string == "### ")
    isListTag ('*':' ':_) = True
    isListTag _ = False
    isQuoteTag ('>':' ':_) = True
    isQuoteTag _ = False


isWhiteSpace :: Char -> Bool
isWhiteSpace ' ' = True
isWhiteSpace '\t' = True
isWhiteSpace _ = False


dropWhile' :: (a -> Bool) -> [a] -> [a]
-- drops the largest initial sublist that satisfies a given predicate
-- dropWhile' even [0,2,4,3,2,3,1] = [3,2,3,1]
dropWhile' _ [] = []
dropWhile' f (x:xs) =
  if f x
  then dropWhile' f xs
  else x:xs


splitWhile :: (a -> Bool) -> [a] -> ([a], [a])
-- splits List where the left result is the largest initial sublist of List satisfying the predicate
splitWhile _ [] = ([], [])
splitWhile f (x:xs) =
  if f x
  then (x:initialSeg, finalSeg)
  else ([], x:xs)
  where
    (initialSeg, finalSeg) = splitWhile f xs
    

parseLinkLine :: String -> String
-- takes a line of gemtext that's a link line and returns a link in html
parseLinkLine ('=':'>':line) = "<p>" ++ "<a href=\"" ++ link ++ "\">" ++ description ++ "</a>" ++ "</p>"
  where
    linkSpaceDescription = (dropWhile' isWhiteSpace line)
    (link, spacesAndDescription) = splitWhile (not . isWhiteSpace) linkSpaceDescription
    description = dropWhile' isWhiteSpace spacesAndDescription
parseLinkLine _ = error "I was supposed to guarantee it was a link line, but pattern matching failed!"


parseHeaderLine :: String -> String
-- takes a line of gemtext that's a header line and returns the approprate <h1> 2 or 3 tag
parseHeaderLine ('#':' ':rest) = "<h1>" ++ rest ++ "</h1>"
parseHeaderLine ('#':'#':' ':rest) = "<h2>" ++ rest ++ "</h2>"
parseHeaderLine ('#':'#':'#':' ':rest) = "<h3>" ++ rest ++ "</h3>"
parseHeaderLine _ = error "I was supposed to guarantee it was a header line, but pattern matching failed!"

parseListLine :: String -> String
parseListLine ('*':' ':rest) = "<li>" ++ rest ++ "</li>"
parseListLine _ = error "I was supposed to guarantee it was a list line, but pattern matching failed!"


parseQuoteLine :: String -> String
parseQuoteLine ('>':' ':rest) = "<blockquote>" ++ rest ++ "</blockquote>"
parseQuoteLine _ = error "I was supposed to guarantee it was a quote line, but pattern matching failed!"

parseParagraphLine :: String -> String
parseParagraphLine line = "<p>" ++ line ++ "</p>"

